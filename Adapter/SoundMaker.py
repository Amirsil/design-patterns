from Adapter.Interface import Interface
from utils import Interface

class SoundMaker(Interface):
    def sound(self) -> None:
        pass